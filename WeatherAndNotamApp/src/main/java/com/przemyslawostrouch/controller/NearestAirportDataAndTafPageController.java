package com.przemyslawostrouch.controller;

import com.przemyslawostrouch.service.TafForNearestAirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;

@RestController
public class NearestAirportDataAndTafPageController {

    TafForNearestAirportService tafForNearestAirportService = new TafForNearestAirportService();

    @Autowired
    public NearestAirportDataAndTafPageController(TafForNearestAirportService tafForNearestAirportService) {
        this.tafForNearestAirportService = tafForNearestAirportService;
    }

    @RequestMapping({"/airportdata/taf/{cityFromUser}"})
    public String nearestAirportDataAndTafPage(@PathVariable("cityFromUser") String cityFromUser) throws MalformedURLException, FileNotFoundException {

        return tafForNearestAirportService.nearestAirportDataAndTafPage(cityFromUser);
    }
}
