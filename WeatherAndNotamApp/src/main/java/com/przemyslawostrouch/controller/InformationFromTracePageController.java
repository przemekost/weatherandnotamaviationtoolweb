package com.przemyslawostrouch.controller;

import com.przemyslawostrouch.service.ActuatorTraceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InformationFromTracePageController {


    ActuatorTraceService actuatorTraceService = new ActuatorTraceService();

    @Autowired
    public InformationFromTracePageController(ActuatorTraceService actuatorTraceService) {
        this.actuatorTraceService = actuatorTraceService;
    }

    @RequestMapping({"/airportdata/request"})
    public String informationFromTrace() {

        return actuatorTraceService.printLastTenRequestInformationFromTrace() + "\n";
    }

}
