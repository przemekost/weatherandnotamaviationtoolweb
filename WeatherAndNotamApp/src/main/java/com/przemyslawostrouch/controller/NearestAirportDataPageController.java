package com.przemyslawostrouch.controller;

import com.przemyslawostrouch.service.AirportDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;

@RestController
public class NearestAirportDataPageController {

    private AirportDataService airportDataService;

    @Autowired
    public void setAirportDataService(AirportDataService airportDataService) {
        this.airportDataService = airportDataService;
    }

    @RequestMapping({"/airportdata/{cityFromUser}"})
    public String nearestAirportDataPage(@PathVariable("cityFromUser") String cityFromUser) throws MalformedURLException, FileNotFoundException {
        return airportDataService.nearestAirportDataPage(cityFromUser);
    }
}
