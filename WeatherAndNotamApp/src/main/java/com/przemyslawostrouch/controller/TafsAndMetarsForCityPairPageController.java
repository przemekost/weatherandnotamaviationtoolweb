package com.przemyslawostrouch.controller;

import com.przemyslawostrouch.service.TafAndMetarForCityPairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;

@RestController
public class TafsAndMetarsForCityPairPageController {


    TafAndMetarForCityPairService tafAndMetarForCityPairService = new TafAndMetarForCityPairService();

    @Autowired
    public TafsAndMetarsForCityPairPageController(TafAndMetarForCityPairService tafAndMetarForCityPairService) {
        this.tafAndMetarForCityPairService = tafAndMetarForCityPairService;
    }

    @RequestMapping({"/airportdata/citypairs/{originIataFromUser}/{destinationIataFromUser}"})
    public String tafsAndMetarsForCityPair(@PathVariable("originIataFromUser") String originIataFromUser
            , @PathVariable("destinationIataFromUser") String destinationIataFromUser) throws MalformedURLException, FileNotFoundException {

        return tafAndMetarForCityPairService.tafsAndMetarsForCityPair(originIataFromUser, destinationIataFromUser);
    }

}
