package com.przemyslawostrouch.controller;

import com.przemyslawostrouch.service.MetarForNearestAirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;

@RestController
public class NearestAirportDataAndMetarPageController {


    MetarForNearestAirportService metarForNearestAirportService = new MetarForNearestAirportService();

    @Autowired
    public NearestAirportDataAndMetarPageController(MetarForNearestAirportService metarForNearestAirportService) {
        this.metarForNearestAirportService = metarForNearestAirportService;
    }

    @RequestMapping({"/airportdata/metar/{cityFromUser}"})
    public String nearestAirportDataAndMetarPage(@PathVariable("cityFromUser") String cityFromUser) throws MalformedURLException, FileNotFoundException {

        return metarForNearestAirportService.nearestAirportDataAndMetarPage(cityFromUser);
    }
}
