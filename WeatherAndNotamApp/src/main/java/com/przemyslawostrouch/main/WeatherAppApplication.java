package com.przemyslawostrouch.main;


import com.przemyslawostrouch.logic.TakeInformationForCityPair;
import com.przemyslawostrouch.parsers.JsonParser;
import com.przemyslawostrouch.parsers.XmlToJson;
import jdk.nashorn.internal.parser.JSONParser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.przemyslawostrouch.controller", "com.przemyslawostrouch.service"})
@SpringBootApplication
public class WeatherAppApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(WeatherAppApplication.class, args);

    }
}
