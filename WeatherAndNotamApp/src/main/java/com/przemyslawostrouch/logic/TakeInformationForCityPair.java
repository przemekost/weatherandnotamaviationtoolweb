package com.przemyslawostrouch.logic;


import com.przemyslawostrouch.domain.Airport;
import com.przemyslawostrouch.domain.CityPair;
import com.przemyslawostrouch.parsers.CsvParser;
import com.przemyslawostrouch.parsers.JsonToObject;
import com.przemyslawostrouch.service.TafAndMetarForCityPairService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import static java.time.ZoneOffset.UTC;

/**
 * Created by Przemek on 2017-07-08.
 */
public class TakeInformationForCityPair {

    TafAndMetarForCityPairService tafAndMetarForCityPairService = new TafAndMetarForCityPairService();

    private String icaoCode;

    public String searchIcaoByIataCode(String iata) {
        CsvParser csvParser = new CsvParser();

        for (Airport airport : csvParser.getListOfAerodromesAsObjects()) {
            if (airport.getIata().equals(iata.toUpperCase())) {
                icaoCode = airport.getIcao();
            }
        }
        return icaoCode;
    }

    public HashMap<String, Long> takeTimeInSeconds(String originIataFromUser, String destinationIataFromUser){
        JsonToObject jsonToObject = new JsonToObject();
        CityPair cityPair = jsonToObject.fromJsonToCityPairObject(originIataFromUser, destinationIataFromUser);
        String departureTime = cityPair.getOrigin().getDate();
        LocalDateTime departureLocalDateTime = LocalDateTime.parse(departureTime, DateTimeFormatter.ISO_DATE_TIME);
        Long departureTimeInSeconds = departureLocalDateTime.toInstant(UTC).toEpochMilli()/1000;

        String destinationTime = cityPair.getDestination().getDate();
        LocalDateTime arrivalLocalDateTime = LocalDateTime.parse(destinationTime, DateTimeFormatter.ISO_DATE_TIME);
        Long arrivalTimeInSeconds = arrivalLocalDateTime.toInstant(UTC).toEpochMilli()/1000;

        HashMap<String,Long> departureAndArrivalTime = new HashMap();
        departureAndArrivalTime.put("departureTime", departureTimeInSeconds);
        departureAndArrivalTime.put("arrivalTime", arrivalTimeInSeconds);


        return departureAndArrivalTime;
    }



}
