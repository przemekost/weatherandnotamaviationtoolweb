package com.przemyslawostrouch.logic;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.przemyslawostrouch.domain.Airport;
import com.przemyslawostrouch.domain.City;
import com.przemyslawostrouch.domain.CityAirportDistance;
import com.przemyslawostrouch.parsers.CsvParser;
import com.przemyslawostrouch.parsers.JsonParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Przemek on 2017-07-03.
 */
public class NearestAirport {

    private City searchedCityObject;
    private String cityFromUrlAddress;
    private CsvParser csvParser = new CsvParser();
    private String jsonString;
    private String indented;

    public NearestAirport() {
    }

    public void setSearchedCity(String cityFromUser) {
        JsonParser jsonParser = new JsonParser();
        cityFromUrlAddress = cityFromUser;
        jsonParser.readLatitudeAndLongitudeFromJsonObject(cityFromUrlAddress);
        searchedCityObject = City.builder()
                .name(searchedCityToUpperCase(cityFromUrlAddress))
                .latitude(jsonParser.getSearchedCityLatitude())
                .longitude(jsonParser.getSearchedCityLongitude())
                .build();
    }


    private List<CityAirportDistance> cityAirportDistanceList;
    private String nearestAirportIcao;

    public String getNearestAirportIcao() {
        return nearestAirportIcao;
    }

    public String searchedCityToUpperCase(String searchedCity) {
        char firstChar = Character.toUpperCase(searchedCity.charAt(0));
        return firstChar + searchedCity.substring(1);
    }


    public double calculateDistance(City searchedCityCoordinates, Airport airport) {

        double cityLongitude = Math.toRadians(searchedCityCoordinates.getLongitude());
        double cityLatitude = Math.toRadians(searchedCityCoordinates.getLatitude());
        double airportLongitude = Math.toRadians(airport.getLongitude());
        double airportLatitude = Math.toRadians(airport.getLatitude());

        double angle1 = Math.acos(Math.sin(cityLatitude) * Math.sin(airportLatitude)
                + Math.cos(cityLatitude) * Math.cos(airportLatitude) * Math.cos(cityLongitude - airportLongitude));

        angle1 = Math.toDegrees(angle1);

        return 60 * angle1;


    }

    public List<CityAirportDistance> calculateDistanceBetweenAllAirportsAndSelectedCity(List<Airport> listOfAerodromesAsObjects, City searchedCity) {
        double distanceBetweenCityAndAirport;

        cityAirportDistanceList = new ArrayList<>();
        for (Airport airport : listOfAerodromesAsObjects) {
            distanceBetweenCityAndAirport = calculateDistance(searchedCity, airport);
            CityAirportDistance cityAirportDistance = CityAirportDistance.builder()
                    .searchedCityName(searchedCity.getName())
                    .airportName(airport.getName())
                    .distanceBetweenSearchedCityAndAirport(distanceBetweenCityAndAirport)
                    .build();
            cityAirportDistanceList.add(cityAirportDistance);

        }
        return cityAirportDistanceList;

    }

    public List<CityAirportDistance> createListOfNearestAirports(City searchedCityAsAObject) {
        calculateDistanceBetweenAllAirportsAndSelectedCity(csvParser.getListOfAerodromesAsObjects(), searchedCityAsAObject);

        cityAirportDistanceList.sort(new Comparator<CityAirportDistance>() {
            @Override
            public int compare(CityAirportDistance c1, CityAirportDistance c2) {
                if (c1.getDistanceBetweenSearchedCityAndAirport() < c2.getDistanceBetweenSearchedCityAndAirport())
                    return -1;
                if (c1.getDistanceBetweenSearchedCityAndAirport() > c2.getDistanceBetweenSearchedCityAndAirport())
                    return 1;
                return 0;
            }
        });

        return cityAirportDistanceList;
    }

    public String parseToJson() {

        String nearestAirport = createListOfNearestAirports(searchedCityObject).get(0).getAirportName();
        Gson gson = new Gson();
        for (Airport airport : csvParser.getListOfAerodromesAsObjects()) {
            if (airport.getName().equals(nearestAirport)) {
                jsonString = gson.toJson(airport);
                nearestAirportIcao = airport.getIcao();
                ObjectMapper mapper = new ObjectMapper();
                Object json = null;
                try {
                    json = mapper.readValue(jsonString, Airport.class);
                    indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return indented;
    }
}
