package com.przemyslawostrouch.service;

import com.przemyslawostrouch.logic.NearestAirport;
import org.springframework.stereotype.Service;

/**
 * Created by Przemek on 2017-07-14.
 */
@Service
public class AirportDataService {
    private String city;


    public AirportDataService(){}

    public String nearestAirportDataPage(String cityFromUser) {

        city = cityFromUser;
        NearestAirport nearestAirport = new NearestAirport();
        nearestAirport.setSearchedCity(city);

        return "Nearest airport data: \n" + nearestAirport.parseToJson();
    }
}
