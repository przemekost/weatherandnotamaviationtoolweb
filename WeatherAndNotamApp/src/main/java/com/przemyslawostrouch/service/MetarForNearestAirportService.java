package com.przemyslawostrouch.service;

import com.przemyslawostrouch.domain.TypeOfInformation;
import com.przemyslawostrouch.logic.NearestAirport;
import com.przemyslawostrouch.parsers.XmlToJson;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;

/**
 * Created by Przemek on 2017-07-14.
 */
@Service
public class MetarForNearestAirportService {
    private String city;
    private String nearestAirportIcao;
    public MetarForNearestAirportService(){}

    public String nearestAirportDataAndMetarPage(String cityFromUser) throws MalformedURLException, FileNotFoundException {

        city = cityFromUser;
        NearestAirport nearestAirport = new NearestAirport();
        XmlToJson xmlToJson = new XmlToJson();
        nearestAirport.setSearchedCity(city);
        nearestAirport.parseToJson();
        nearestAirportIcao = nearestAirport.getNearestAirportIcao();

        return "Recent METAR for the nearest airport of the city: \n" + xmlToJson.fromXmlToJson(TypeOfInformation.METAR, nearestAirportIcao);
    }
}
