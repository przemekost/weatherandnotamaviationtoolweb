package com.przemyslawostrouch.service;

import com.przemyslawostrouch.staticVariables.StaticValues;


/**
 * Created by Przemek on 2017-07-11.
 */
public class UrlAddressService {

    TafAndMetarForCityPairService tams = new TafAndMetarForCityPairService();
    private Long departureTime;
    private Long arrivalTime;

    public void setTams(TafAndMetarForCityPairService tams) {
        this.tams = tams;
    }

    public String createUrlForMetar() {

        departureTime = tams.getDepartureAndArrivalTime().get("departureTime");
        arrivalTime = tams.getDepartureAndArrivalTime().get("arrivalTime");

        return new StringBuilder()
                .append(StaticValues.MAIN_URL_ADDRESS_FOR_CITY_PAIRS)
                .append(departureTime)
                .append(StaticValues.END_TIME_STRING)
                .append(arrivalTime)
                .append(StaticValues.STATION_STRING)
                .append(tams.getOriginIcao())
                .append(",")
                .append(tams.getDestinationIcao())
                .append(StaticValues.MOST_RECENT_FOR_TYPED_STATIONS_METARS)
                .toString();
    }

    public String createUrlForTaf() {

        departureTime = tams.getDepartureAndArrivalTime().get("departureTime");
        arrivalTime = tams.getDepartureAndArrivalTime().get("arrivalTime");
        return new StringBuilder()
                .append(StaticValues.MAIN_URL_ADDRESS_FOR_CITY_PAIRS_TAFS)
                .append(departureTime)
                .append(StaticValues.END_TIME_STRING)
                .append(arrivalTime)
                .append(StaticValues.STATION_STRING)
                .append(tams.getOriginIcao())
                .append(",%20")
                .append(tams.getDestinationIcao())
                .append(StaticValues.MOST_RECENT_FOR_TYPED_STATIONS_TAFS)
                .toString();
    }


}
