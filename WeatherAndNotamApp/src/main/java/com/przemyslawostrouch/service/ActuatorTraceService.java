package com.przemyslawostrouch.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.przemyslawostrouch.domain.LastRequests;
import com.przemyslawostrouch.domain.RequestsInformationFromTrace;
import com.przemyslawostrouch.domain.Timestamp;
import com.przemyslawostrouch.parsers.JsonParser;
import com.przemyslawostrouch.staticVariables.StaticValues;
import org.json.JSONArray;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Przemek on 2017-07-16.
 */
@Service
public class ActuatorTraceService {

    private JsonParser jsonParser = new JsonParser();
    private List<LastRequests> listWithInformationFromTrace = new ArrayList<>();
    private JSONArray jsonArrayFromString;
    private JSONArray listWithoutRequestAndTracePath;


    public void takeInformationFromActuatorTrace() {
        jsonArrayFromString = new JSONArray("[{}]");
        String urlAddressForTrace = "http://localhost:8080/trace";
        jsonArrayFromString = new JSONArray(jsonParser.takeJsonAsAStringFromUrl(urlAddressForTrace));
    }

    public String createPathForCheck(int i) {
        return jsonArrayFromString
                .getJSONObject(i)
                .getJSONObject("info")
                .get("path")
                .toString();
    }

    public void createListWithoutRequestAndTracePath() {
        listWithInformationFromTrace.clear();
        takeInformationFromActuatorTrace();
        listWithoutRequestAndTracePath = new JSONArray();

        for (int i = 0; i < jsonArrayFromString.length(); i++) {
            if (!createPathForCheck(i).equals("/airportdata/request") &&
                    !createPathForCheck(i).equals("/trace")) {
                listWithoutRequestAndTracePath.put(jsonArrayFromString.get(i));
            }
        }
    }


    public void createListWithInformationsFromTrace() {
        createListWithoutRequestAndTracePath();
        for (int i = 0; i < listWithoutRequestAndTracePath.length(); i++) {
            LastRequests lastRequests = LastRequests
                    .builder()
                    .timestamp(takeTimestampFromTrace(i))
                    .requestsInformationFromTrace(takeRequestInformationFromTrace(i))
                    .build();
            listWithInformationFromTrace.add(lastRequests);
        }
    }


    public RequestsInformationFromTrace takeRequestInformationFromTrace(int i) {

        com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();


        String basicString = listWithoutRequestAndTracePath
                .getJSONObject(i)
                .getJSONObject("info")
                .getJSONObject("headers")
                .get("request") + "";
        JsonObject o = jsonParser.parse(basicString).getAsJsonObject();

        return RequestsInformationFromTrace
                .builder()
                .acceptLanguage(o.get("accept-language").toString())
                .postmanToken(o.get("postman-token").toString())
                .host(o.get("host").toString())
                .connection(o.get("connection").toString())
                .contentType(o.get("content-type").toString())
                .cacheControl(o.get("cache-control").toString())
                .acceptEncoding(o.get("accept-encoding").toString())
                .userAgent(o.get("user-agent").toString())
                .accept(o.get("accept").toString())
                .build();
    }


    public Timestamp takeTimestampFromTrace(int i) {

        return Timestamp
                .builder()
                .timestamp(listWithoutRequestAndTracePath.getJSONObject(i).get("timestamp").toString())
                .build();
    }


    public String printLastTenRequestInformationFromTrace() {
        StringBuilder stringToPrint = new StringBuilder();
        createListWithInformationsFromTrace();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        if (listWithInformationFromTrace.size() > 10) {
            for (int i = 0; i < StaticValues.NUMBER_POSITION_TO_PRINT; i++) {
                stringToPrint.append(gson.toJson(listWithInformationFromTrace.get(i))).append("\n");
            }
        } else {
            for (LastRequests aListWithInformationFromTrace : listWithInformationFromTrace) {
                stringToPrint.append(gson.toJson(aListWithInformationFromTrace)).append("\n");
            }
        }

        return stringToPrint.toString();
    }

}



