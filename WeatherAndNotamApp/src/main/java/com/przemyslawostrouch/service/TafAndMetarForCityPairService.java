package com.przemyslawostrouch.service;

import com.przemyslawostrouch.domain.TypeOfInformation;
import com.przemyslawostrouch.logic.TakeInformationForCityPair;
import com.przemyslawostrouch.parsers.XmlToJson;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.HashMap;

/**
 * Created by Przemek on 2017-07-14.
 */
@Service
public class TafAndMetarForCityPairService {

    private static String originIcao;
    private static String destinationIcao;
    private static HashMap<String, Long> departureAndArrivalTime;

    public static String getOriginIcao() {
        return originIcao;
    }

    public static String getDestinationIcao() {
        return destinationIcao;
    }

    public static HashMap<String, Long> getDepartureAndArrivalTime() {
        return departureAndArrivalTime;
    }

    public TafAndMetarForCityPairService() {
    }

    public String tafsAndMetarsForCityPair(String originIataFromUser,
                                           String destinationIataFromUser) throws MalformedURLException, FileNotFoundException {


        TakeInformationForCityPair takeInfo = new TakeInformationForCityPair();
        originIcao = takeInfo.searchIcaoByIataCode(originIataFromUser);
        destinationIcao = takeInfo.searchIcaoByIataCode(destinationIataFromUser);


        originIataFromUser = originIataFromUser.toUpperCase();
        destinationIataFromUser = destinationIataFromUser.toUpperCase();
        departureAndArrivalTime = takeInfo.takeTimeInSeconds(originIataFromUser, destinationIataFromUser);

        XmlToJson xmlToJson = new XmlToJson();
        String metarsForCityPair = xmlToJson.fromXmlToJson(TypeOfInformation.CITY_PAIR_METAR, "");

        String tafsForCityPair = xmlToJson.fromXmlToJson(TypeOfInformation.CITY_PAIR_TAFS, "");


        return "METARs for given city pair: \n----------------------------------------------\n"
                + metarsForCityPair + "\n----------------------------------------------\n"
                + "TAFs for given city pair: \n" + tafsForCityPair;
    }
}
