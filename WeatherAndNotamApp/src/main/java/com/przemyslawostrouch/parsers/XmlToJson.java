package com.przemyslawostrouch.parsers;

import com.przemyslawostrouch.domain.TypeOfInformation;
import com.przemyslawostrouch.staticVariables.StaticValues;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;

/**
 * Created by Przemek on 2017-07-08.
 */
public class XmlToJson {

    private String stringToPrint;

    public String fromXmlToJson(TypeOfInformation typeOfInformation, String icao) throws MalformedURLException, FileNotFoundException {
        XmlParser xmlParser = new XmlParser();

        String xmlString = xmlParser.takeXmlFromApi(typeOfInformation, icao);
        JSONObject jsonObject = XML.toJSONObject(xmlString);


        if (typeOfInformation.equals(TypeOfInformation.CITY_PAIR_METAR)) {
            try {
                takeARawTextElementFromNotice("METAR", jsonObject);
            } catch (JSONException je) {
                System.out.println(je.toString());
            }

        } else if (typeOfInformation.equals(TypeOfInformation.CITY_PAIR_TAFS)) {
            try {
                takeARawTextElementFromNotice("TAF", jsonObject);
            } catch (JSONException je) {
                System.out.println(je.toString());
            }

        } else if (typeOfInformation.equals(TypeOfInformation.METAR) && numberOfNoticeForAirport(jsonObject) != 0) {
            try {
                stringToPrint = jsonObject.getJSONObject("response").getJSONObject("data").getJSONObject("METAR").toString(StaticValues.PRETTY_PRINT_INDENT_FACTOR);
            } catch (JSONException je) {
                System.out.println(je.toString());
            }
        } else if (typeOfInformation.equals(TypeOfInformation.TAF) && numberOfNoticeForAirport(jsonObject) != 0) {
            try {
                stringToPrint = jsonObject.getJSONObject("response").getJSONObject("data").getJSONArray("TAF").toString(StaticValues.PRETTY_PRINT_INDENT_FACTOR);
            } catch (JSONException je) {
                System.out.println(je.toString());
            }
        } else {
            stringToPrint = "There are no any searched notice at this airport (" + icao + ")";
        }

        return stringToPrint;
    }


    public int numberOfNoticeForAirport(JSONObject jsonObject) {
        return (int) jsonObject.getJSONObject("response").getJSONObject("data").get("num_results");
    }


    public String takeARawTextElementFromNotice(String keyForJsonArray, JSONObject jsonObject) {

        stringToPrint = "";
        JSONArray jsonArray = jsonObject.getJSONObject("response").getJSONObject("data").getJSONArray(keyForJsonArray);
        for (int i = 0; i < jsonArray.length(); i++) {
            stringToPrint = stringToPrint + jsonArray.getJSONObject(i).get("raw_text") + "\n";
        }
        return stringToPrint;
    }
}
