package com.przemyslawostrouch.parsers;

import com.przemyslawostrouch.staticVariables.StaticValues;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Przemek on 2017-06-27.
 */
public class JsonParser {

    private JSONObject objFromString;
    private double searchedCityLatitude;
    private double searchedCityLongitude;
    private String jsonString;

    public double getSearchedCityLatitude() {
        return searchedCityLatitude;
    }

    public double getSearchedCityLongitude() {
        return searchedCityLongitude;
    }

    public JsonParser() {

    }

    public void fromJsonToCityObject(String searchedCity) {

        //Google API used
        String urlAddressForFindCityCoordinates = StaticValues.MAIN_URL_ADDRESS_FOR_CITY_COORDINATES + searchedCity;

        objFromString = new JSONObject(takeJsonAsAStringFromUrl(urlAddressForFindCityCoordinates));

    }

    public void readLatitudeAndLongitudeFromJsonObject(String searchedCity) {
        fromJsonToCityObject(searchedCity);
        JSONArray array = objFromString.getJSONArray("results");
        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);

            String stringLatitude = ((JSONObject) object.get("geometry")).getJSONObject("location").get("lat").toString();
            searchedCityLatitude = Double.parseDouble(stringLatitude);
            String stringLongitude = ((JSONObject) object.get("geometry")).getJSONObject("location").get("lng").toString();
            searchedCityLongitude = Double.parseDouble(stringLongitude);


        }
    }

    public String takeJsonAsAStringFromUrl(String urlAddress){
        try {
            URL url = new URL(urlAddress);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            jsonString = IOUtils.toString(connection.getInputStream());
        } catch (IOException e) {
            System.err.print("Wrong URL format");
        }
        return jsonString;
    }
}

