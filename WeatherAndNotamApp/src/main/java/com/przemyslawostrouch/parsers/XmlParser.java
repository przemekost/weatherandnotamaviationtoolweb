package com.przemyslawostrouch.parsers;

import com.przemyslawostrouch.domain.TypeOfInformation;
import com.przemyslawostrouch.service.UrlAddressService;
import com.przemyslawostrouch.staticVariables.StaticValues;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * Created by Przemek on 2017-07-03.
 */
public class XmlParser {

    private StringBuilder xmlFile;
    private String urlAddress;
    UrlAddressService addressService = new UrlAddressService();


    public String takeXmlFromApi(TypeOfInformation typeOfInformation, String icao) {

        createUrlAddress(typeOfInformation, icao);

        String line = null;
        URL url = null;
        try {
            url = new URL(urlAddress);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            System.err.print("URL is not valid");
        }
        HttpURLConnection connection = null;
        try {
            xmlFile = null;
            connection = (HttpURLConnection) url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            xmlFile = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                xmlFile.append(line).append("\n");
            }

            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return xmlFile.toString();
    }

    public String createUrlAddress(TypeOfInformation typeOfInformation, String icao) {
        switch (typeOfInformation) {

            case METAR:
                urlAddress = StaticValues.MAIN_URL_ADDRESS_FOR_METAR + icao;
                break;
            case TAF:
                urlAddress = StaticValues.MAIN_URL_ADDRESS_FOR_TAF + icao + StaticValues.TAF_4_HOURS_BEFORE_NOW;
                break;
            case CITY_PAIR_METAR:
                urlAddress = addressService.createUrlForMetar();
                break;
            case CITY_PAIR_TAFS:
                urlAddress = addressService.createUrlForTaf();
                break;
            default:
                System.out.println("Wrong Type of informations");
        }
        return urlAddress;
    }


//    @Override
//    public void unmarshalFileFromXmlToObject(String xmlFile) throws Exception {
//
//
//        JAXBContext jaxbContext = JAXBContext.newInstance(Data.class);
//        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
//
//        StringReader reader = new StringReader(xmlFile);
//        Data metarData = (Data) unmarshaller.unmarshal(reader);
//
//        System.out.println(metarData.getMetars().get(0).getStationId());
//    }


//    public void generateXml() throws JAXBException {
//        Metar metar = Metar.builder()
//                .rawText("EPKT")
//                .windSpeed(30)
//                .build();
//
//
//        Metar metar1 = Metar.builder()
//                .rawText("EPWA")
//                .windSpeed(99)
//                .build();
//
//        List<Metar> metars = new ArrayList<>();
//        metars.add(metar);
//        metars.add(metar1);
//        Data data = new Data(1,metars);
//
//        File file = new File("metars.xml");
//        JAXBContext jaxbContext = JAXBContext.newInstance(Data.class);
//        Marshaller marshaller = jaxbContext.createMarshaller();
//
//        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//
//        marshaller.marshal(data, file);
//        marshaller.marshal(data, System.out);
//
//    }
}
