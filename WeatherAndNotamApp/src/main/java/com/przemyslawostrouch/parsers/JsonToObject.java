package com.przemyslawostrouch.parsers;

import com.google.gson.Gson;
import com.przemyslawostrouch.domain.CityPair;
import com.przemyslawostrouch.domain.CityPairs;
import com.przemyslawostrouch.staticVariables.StaticValues;


/**
 * Created by Przemek on 2017-07-08.
 */
public class JsonToObject {

    private CityPair cityPairForPrintAllInformations;

    public CityPair fromJsonToCityPairObject(String originAirportIata, String destinationAirportIata) {

        Gson gson = new Gson();

        CityPairs cityPairs = gson.fromJson(StaticValues.jsonStringForCityPairs, CityPairs.class);

        if (cityPairs != null) {
            for (CityPair cityPair : cityPairs.getCityPairList()) {
                if (cityPair.getOrigin().getAirport().equals(originAirportIata) &&
                        cityPair.getDestination().getAirport().equals(destinationAirportIata)) {
                    cityPairForPrintAllInformations = cityPair;
                }
            }
        }
        return cityPairForPrintAllInformations;
    }
}
