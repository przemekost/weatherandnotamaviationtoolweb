package com.przemyslawostrouch.parsers;

import com.google.gson.Gson;
import com.opencsv.CSVReader;
import com.przemyslawostrouch.domain.Airport;
import com.przemyslawostrouch.domain.City;
import com.przemyslawostrouch.domain.CityAirportDistance;
import com.przemyslawostrouch.staticVariables.StaticValues;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Przemysław on 2017-05-17.
 */
public class CsvParser {

    private List<Airport> listOfAerodromesAsObjects = new ArrayList<>();

    public List<Airport> getListOfAerodromesAsObjects() {
        return listOfAerodromesAsObjects;
    }

    public CsvParser() {
        try {
            fromCsvToListOfObjects();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void fromCsvToListOfObjects() throws IOException {
        try {
            URL url = new URL(StaticValues.URL_ADDRESS_FOR_AIRPORT_DATA);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            CSVReader reader = new CSVReader(new InputStreamReader(connection.getInputStream()));
            String[] tab = null;
            while ((tab = reader.readNext()) != null) {
                parseCsvFileToTheListOfObjects(tab);
            }
        } catch (java.io.IOException e) {
            e.printStackTrace();
            throw e;
        }

    }

    private void parseCsvFileToTheListOfObjects(String[] lineFromCsvFile) {

        Airport airport = Airport.builder()
                .airportId(Integer.parseInt(lineFromCsvFile[0]))
                .name(lineFromCsvFile[1])
                .city(lineFromCsvFile[2])
                .country(lineFromCsvFile[3])
                .iata(lineFromCsvFile[4])
                .icao(lineFromCsvFile[5])
                .latitude(Double.parseDouble(lineFromCsvFile[6]))
                .longitude(Double.parseDouble(lineFromCsvFile[7]))
                .altitude(Double.parseDouble(lineFromCsvFile[8]))
                .timeZone(lineFromCsvFile[9])
                .dst(lineFromCsvFile[10])
                .tzDatabaseTime(lineFromCsvFile[11])
                .type(lineFromCsvFile[12])
                .source(lineFromCsvFile[13])
                .build();
        listOfAerodromesAsObjects.add(airport);


    }


}
