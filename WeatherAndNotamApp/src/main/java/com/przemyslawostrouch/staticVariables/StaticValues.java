package com.przemyslawostrouch.staticVariables;

/**
 * Created by Przemek on 2017-06-25.
 */
public class StaticValues {


    public static final int PRETTY_PRINT_INDENT_FACTOR = 4;
    public static final int NUMBER_POSITION_TO_PRINT = 10;
    public static final String MAIN_URL_ADDRESS_FOR_CITY_COORDINATES = "http://maps.google.cn/maps/api/geocode/json?address=";
    public static final String URL_ADDRESS_FOR_AIRPORT_DATA =
            "https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat";

    public static final String MAIN_URL_ADDRESS_FOR_METAR =
            "https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&hoursBeforeNow=3&mostRecent=true&stationString=";

    public static final String MAIN_URL_ADDRESS_FOR_TAF =
            "https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=tafs&requestType=retrieve&format=xml&stationString=";
    public static final String TAF_4_HOURS_BEFORE_NOW =
            "&hoursBeforeNow=4";

    public static final String MAIN_URL_ADDRESS_FOR_CITY_PAIRS =
            "https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&startTime=";


    // String for construct METAR URL CITY PAIRS
    public static final String END_TIME_STRING = "&endTime=";
    public static final String STATION_STRING = "&stationString=";
    public static final String MOST_RECENT_FOR_TYPED_STATIONS_METARS =
            "&mostRecentForEachStation=true";

    // String for construct TAF URL CITY PAIRS
    public static final String MAIN_URL_ADDRESS_FOR_CITY_PAIRS_TAFS =
            "https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=tafs&requestType=retrieve&format=xml&startTime=";
    public static final String MOST_RECENT_FOR_TYPED_STATIONS_TAFS =
            "&mostRecentForEachStation=postfilter";


    public static final String jsonStringForCityPairs = "{\n" +
            "\"city-pairs\": [{\n" +
            "\"origin\": {\n" +
            "\"airport\": \"WAW\",\n" +
            "\"ETD\": \"2017-07-12T14:20:49+00:00\"\n" +
            "},\n" +
            "\"destination\": {\n" +
            "\"airport\": \"FRA\",\n" +
            "\"ETA\": \"2017-07-12T19:20:49+00:00\"\n" +
            "}\n" +
            "}, {\n" +
            "\"origin\": {\n" +
            "\"airport\": \"FRA\",\n" +
            "\"ETD\": \"2017-07-12T14:20:49+00:00\"\n" +
            "},\n" +
            "\"destination\": {\n" +
            "\"airport\": \"GDN\",\n" +
            "\"ETA\": \"2017-07-12T16:00:49+00:00\"\n" +
            "}\n" +
            "},\n" +
            "{\n" +
            "\"origin\": {\n" +
            "\"airport\": \"GDN\",\n" +
            "\"ETD\": \"2017-07-12T14:20:49+00:00\"\n" +
            "},\n" +
            "\"destination\": {\n" +
            "\"airport\": \"WAW\",\n" +
            "\"ETA\": \"2017-07-12T14:50:49+00:00\"\n" +
            "}\n" +
            "}\n" +
            "]\n" +
            "}";


}


