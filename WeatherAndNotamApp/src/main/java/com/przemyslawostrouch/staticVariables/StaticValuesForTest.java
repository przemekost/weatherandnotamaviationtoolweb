package com.przemyslawostrouch.staticVariables;

/**
 * Created by Przemek on 2017-07-10.
 */
public class StaticValuesForTest {

    public static final String cityNameNumberOneForTest = "katowice";
    public static final String cityNameNumberTwoForTest = "boston";
    public static final String cityNameNumberThreeForTest = "warszawa";


    public static final String URL_ADDRESS_FOR_METAR = "https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=" +
            "metars&requestType=retrieve&format=xml&hoursBeforeNow=3&mostRecent=true&stationString=EPGD";
    public static final String URL_ADDRESS_FOR_TAF = "https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=" +
            "tafs&requestType=retrieve&format=xml&stationString=EPGD&hoursBeforeNow=4";
    public static final String URL_ADDRESS_FOR_CITY_PAIR_METAR ="https://aviationweather.gov/adds/dataserver_current/httpparam?" +
            "dataSource=metars&requestType=retrieve&format=xml&startTime=1499264449&endTime=1499282449&stationString=EPWA" +
            ",EDDF&mostRecentForEachStation=true";
    public static final String URL_ADDRESS_FOR_CITY_PAIR_TAF = "https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=" +
            "tafs&requestType=retrieve&format=xml&startTime=1499264449&endTime=1499282449&stationString=EPWA," +
            "%20EDDF&mostRecentForEachStation=postfilter";

    public static final String jsonMetarStringForTest = "{\n" +
            "  \"response\": {\n" +
            "    \"xmlns:xsd\": \"http://www.w3.org/2001/XMLSchema\",\n" +
            "    \"request\": {\n" +
            "      \"type\": \"retrieve\"\n" +
            "    },\n" +
            "    \"data\": {\n" +
            "      \"METAR\": {\n" +
            "        \"raw_text\": \"EPKT 111800Z 32007KT 280V340 9999 -SHRA SCT046CB 19/15 Q1012\",\n" +
            "        \"elevation_m\": 284,\n" +
            "        \"visibility_statute_mi\": 6.21,\n" +
            "        \"flight_category\": \"VFR\",\n" +
            "        \"observation_time\": \"2017-07-11T18:00:00Z\",\n" +
            "        \"sky_condition\": {\n" +
            "          \"cloud_base_ft_agl\": 4600,\n" +
            "          \"sky_cover\": \"SCT\"\n" +
            "        },\n" +
            "        \"dewpoint_c\": 15,\n" +
            "        \"wind_dir_degrees\": 320,\n" +
            "        \"station_id\": \"EPKT\",\n" +
            "        \"latitude\": 50.47,\n" +
            "        \"temp_c\": 19,\n" +
            "        \"wx_string\": \"-SHRA\",\n" +
            "        \"wind_speed_kt\": 7,\n" +
            "        \"metar_type\": \"METAR\",\n" +
            "        \"altim_in_hg\": 29.88189,\n" +
            "        \"longitude\": 19.08\n" +
            "      },\n" +
            "      \"num_results\": 1\n" + // <--------- this value will be tested
            "    },\n" +
            "    \"xsi:noNamespaceSchemaLocation\": \"http://aviationweather.gov/adds/schema/metar1_2.xsd\",\n" +
            "    \"request_index\": 159455689,\n" +
            "    \"warnings\": \"\",\n" +
            "    \"time_taken_ms\": 4,\n" +
            "    \"xmlns:xsi\": \"http://www.w3.org/2001/XML-Schema-instance\",\n" +
            "    \"version\": 1.2,\n" +
            "    \"data_source\": {\n" +
            "      \"name\": \"metars\"\n" +
            "    },\n" +
            "    \"errors\": \"\"\n" +
            "  }\n" +
            "}";
    public static final String jsonMetarStringCityPairForTest =
    "{\n"+
            "  \"response\": {\n"+
            "    \"xmlns:xsd\": \"http://www.w3.org/2001/XMLSchema\",\n"+
            "    \"request\": {\n"+
            "      \"type\": \"retrieve\"\n"+
            "    },\n"+
            "    \"data\": {\n"+
            "      \"TAF\": [\n"+
            "        {\n"+
            "          \"raw_text\": \"TAF EDDF 051734Z 0518/0624 33008KT CAVOK BECMG 0518/0521 03004KT BECMG 0610/0612 22005KT PROB30 TEMPO 0616/0624 22015G25KT TSRA BKN045CB\",\n"+
            "          \"elevation_m\": 113,\n"+
            "          \"valid_time_to\": \"2017-07-07T00:00:00Z\",\n"+
            "          \"valid_time_from\": \"2017-07-05T18:00:00Z\",\n"+
            "          \"station_id\": \"EDDF\",\n"+
            "          \"latitude\": 50.05,\n"+
            "          \"bulletin_time\": \"2017-07-05T17:00:00Z\",\n"+
            "          \"forecast\": [\n"+
            "            {\n"+
            "              \"visibility_statute_mi\": 6.21,\n"+
            "              \"sky_condition\": {\n"+
            "                \"sky_cover\": \"NSC\"\n"+
            "              },\n"+
            "              \"wind_dir_degrees\": 330,\n"+
            "              \"fcst_time_from\": \"2017-07-05T18:00:00Z\",\n"+
            "              \"fcst_time_to\": \"2017-07-07T00:00:00Z\",\n"+
            "              \"wx_string\": \"NSW\",\n"+
            "              \"wind_speed_kt\": 8\n"+
            "            },\n"+
            "            {\n"+
            "              \"visibility_statute_mi\": 6.21,\n"+
            "              \"sky_condition\": {\n"+
            "                \"sky_cover\": \"NSC\"\n"+
            "              },\n"+
            "              \"time_becoming\": \"2017-07-05T21:00:00Z\",\n"+
            "              \"wind_dir_degrees\": 30,\n"+
            "              \"change_indicator\": \"BECMG\",\n"+
            "              \"fcst_time_from\": \"2017-07-05T18:00:00Z\",\n"+
            "              \"fcst_time_to\": \"2017-07-06T10:00:00Z\",\n"+
            "              \"wx_string\": \"NSW\",\n"+
            "              \"wind_speed_kt\": 4\n"+
            "            },\n"+
            "            {\n"+
            "              \"visibility_statute_mi\": 6.21,\n"+
            "              \"sky_condition\": {\n"+
            "                \"sky_cover\": \"NSC\"\n"+
            "              },\n"+
            "              \"time_becoming\": \"2017-07-06T12:00:00Z\",\n"+
            "              \"wind_dir_degrees\": 220,\n"+
            "              \"change_indicator\": \"BECMG\",\n"+
            "              \"fcst_time_from\": \"2017-07-06T10:00:00Z\",\n"+
            "              \"fcst_time_to\": \"2017-07-07T00:00:00Z\",\n"+
            "              \"wx_string\": \"NSW\",\n"+
            "              \"wind_speed_kt\": 5\n"+
            "            },\n"+
            "            {\n"+
            "              \"sky_condition\": {\n"+
            "                \"cloud_base_ft_agl\": 4500,\n"+
            "                \"cloud_type\": \"CB\",\n"+
            "                \"sky_cover\": \"BKN\"\n"+
            "              },\n"+
            "              \"wind_dir_degrees\": 220,\n"+
            "              \"change_indicator\": \"TEMPO\",\n"+
            "              \"probability\": 30,\n"+
            "              \"fcst_time_from\": \"2017-07-06T16:00:00Z\",\n"+
            "              \"fcst_time_to\": \"2017-07-07T00:00:00Z\",\n"+
            "              \"wind_gust_kt\": 25,\n"+
            "              \"wx_string\": \"TSRA\",\n"+
            "              \"wind_speed_kt\": 15\n"+
            "            }\n"+
            "          ],\n"+
            "          \"issue_time\": \"2017-07-05T17:34:00Z\",\n"+
            "          \"longitude\": 8.6\n"+
            "        },\n"+
            "        {\n"+
            "          \"raw_text\": \"TAF EPWA 051730Z 0518/0618 32005KT CAVOK PROB40 0602/0604 3000 BR\",\n"+
            "          \"elevation_m\": 107,\n"+
            "          \"valid_time_to\": \"2017-07-06T18:00:00Z\",\n"+
            "          \"valid_time_from\": \"2017-07-05T18:00:00Z\",\n"+
            "          \"station_id\": \"EPWA\",\n"+
            "          \"latitude\": 52.17,\n"+
            "          \"bulletin_time\": \"2017-07-05T17:00:00Z\",\n"+
            "          \"forecast\": [\n"+
            "            {\n"+
            "              \"visibility_statute_mi\": 6.21,\n"+
            "              \"sky_condition\": {\n"+
            "                \"sky_cover\": \"NSC\"\n"+
            "              },\n"+
            "              \"wind_dir_degrees\": 320,\n"+
            "              \"fcst_time_from\": \"2017-07-05T18:00:00Z\",\n"+
            "              \"fcst_time_to\": \"2017-07-06T18:00:00Z\",\n"+
            "              \"wx_string\": \"NSW\",\n"+
            "              \"wind_speed_kt\": 5\n"+
            "            },\n"+
            "            {\n"+
            "              \"visibility_statute_mi\": 1.86,\n"+
            "              \"change_indicator\": \"PROB\",\n"+
            "              \"probability\": 40,\n"+
            "              \"fcst_time_from\": \"2017-07-06T02:00:00Z\",\n"+
            "              \"fcst_time_to\": \"2017-07-06T04:00:00Z\",\n"+
            "              \"wx_string\": \"BR\"\n"+
            "            }\n"+
            "          ],\n"+
            "          \"issue_time\": \"2017-07-05T17:30:00Z\",\n"+
            "          \"longitude\": 20.97\n"+
            "        }\n"+
            "      ],\n"+
            "      \"num_results\": 2\n"+
            "    },\n"+
            "    \"xsi:noNamespaceSchemaLocation\": \"http://aviationweather.gov/adds/schema/taf1_2.xsd\",\n"+
            "    \"request_index\": 159760852,\n"+
            "    \"warnings\": \"\",\n"+
            "    \"time_taken_ms\": 17,\n"+
            "    \"xmlns:xsi\": \"http://www.w3.org/2001/XML-Schema-instance\",\n"+
            "    \"version\": 1.2,\n"+
            "    \"data_source\": {\n"+
            "      \"name\": \"tafs\"\n"+
            "    },\n"+
            "    \"errors\": \"\"\n"+
            "  }\n"+
            "}";

    public static final String jsonStringForCityPairsTest = "{\n" +
            "\"city-pairs\": [{\n" +
            "\"origin\": {\n" +
            "\"airport\": \"GDN\",\n" +
            "\"ETD\": \"2017-07-05T14:20:49+00:00\"\n" +
            "},\n" +
            "\"destination\": {\n" +
            "\"airport\": \"WAW\",\n" +
            "\"ETA\": \"2017-07-05T14:50:49+00:00\"\n" +
            "}\n" +
            "}\n" +
            "]\n" +
            "}";
}
