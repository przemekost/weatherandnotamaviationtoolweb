package com.przemyslawostrouch.domain;


import com.google.gson.annotations.SerializedName;

/**
 * Created by Przemek on 2017-07-08.
 */
@lombok.Data
public class Destination {
    private String airport;
    @SerializedName("ETA")
    private String date;
}
