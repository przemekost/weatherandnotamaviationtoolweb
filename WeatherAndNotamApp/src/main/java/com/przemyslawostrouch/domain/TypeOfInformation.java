package com.przemyslawostrouch.domain;

/**
 * Created by Przemek on 2017-07-08.
 */
public enum TypeOfInformation {

    METAR,
    TAF,
    CITY_PAIR_METAR,
    CITY_PAIR_TAFS


}
