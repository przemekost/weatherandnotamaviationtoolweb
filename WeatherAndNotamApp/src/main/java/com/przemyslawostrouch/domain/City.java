package com.przemyslawostrouch.domain;

import lombok.Builder;
import lombok.Data;

/**
 * Created by Przemek on 2017-06-25.
 */
@Data
@Builder
public class City {

    private String name;
    private double longitude;
    private double latitude;

}
