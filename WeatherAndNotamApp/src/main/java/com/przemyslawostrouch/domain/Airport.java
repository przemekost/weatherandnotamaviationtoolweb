package com.przemyslawostrouch.domain;

import lombok.Builder;
import lombok.Data;

/**
 * Created by Przemek on 2017-06-18.
 */
@Builder
@Data
public class Airport {

        private final int airportId;
        private final String name;
        private final String city;
        private final String country;
        private final String iata;
        private final String icao;
        private final double latitude;
        private final double longitude;
        private final double altitude;
        private final String timeZone;
        private final String dst;
        private final String tzDatabaseTime;
        private final String type;
        private final String source;
}
