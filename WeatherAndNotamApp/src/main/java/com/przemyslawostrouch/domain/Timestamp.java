package com.przemyslawostrouch.domain;

import lombok.Builder;
import lombok.Data;

/**
 * Created by Przemek on 2017-07-17.
 */
@Data
@Builder
public class Timestamp {

    private String timestamp;

}
