

package com.przemyslawostrouch.domain;

import lombok.*;
import lombok.experimental.Tolerate;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by Przemek on 2017-07-08.
 */
@lombok.Data
@Builder
@XmlRootElement(name = "response", namespace = "http://www.w3.org/2001/XMLSchem")
@XmlType(name = "response", propOrder = {"requestIndex", "dataSource", "dataSource", "errors", "warnings", "timeTakenMs", "data"})
public class Response {

    @Tolerate
    public Response(){}

    @XmlElement(name = "request_index", namespace = "http://www.w3.org/2001/XMLSchem")
    protected long requestIndex;

    @XmlElement(name="data_source")
    protected String dataSource;

    @XmlElement(name="request")
    protected String c;

    @XmlElement(name="errors")
    protected String errors;

    @XmlElement(name="warnings")
    protected String warnings;

    @XmlElement(name="time_taken_ms")
    protected int timeTakenMs;

    @XmlElement(name = "data")
    protected int data;
}
