
package com.przemyslawostrouch.domain;

import lombok.Builder;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by Przemek on 2017-07-05.
 */
@XmlRootElement(name = "data")
@XmlAccessorType(XmlAccessType.FIELD)
public class Data {

    public Data(){}

    public Data(int numResult, List<Metar> metars) {
        this.numResult = numResult;
        this.metars = metars;
    }

    public int getNumResult() {
        return numResult;
    }

    public void setNumResult(int numResult) {
        this.numResult = numResult;
    }

    public List<Metar> getMetars() {
        return metars;
    }

    public void setMetars(List<Metar> metars) {
        this.metars = metars;
    }

    @XmlAttribute(name="num_results")
    private int numResult;
    @XmlElement(name = "METAR")
    private List<Metar> metars ;

//    @XmlAttribute
//    private String num_results;
}
