package com.przemyslawostrouch.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LastRequests {

    private Timestamp timestamp;
    private RequestsInformationFromTrace requestsInformationFromTrace;
}
