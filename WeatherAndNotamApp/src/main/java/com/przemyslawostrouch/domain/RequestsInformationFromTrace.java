package com.przemyslawostrouch.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RequestsInformationFromTrace {

    private String acceptLanguage;
    private String postmanToken;
    private String host;
    private String connection;
    private String contentType;
    private String cacheControl;
    private String acceptEncoding;
    private String userAgent;
    private String accept;
}
