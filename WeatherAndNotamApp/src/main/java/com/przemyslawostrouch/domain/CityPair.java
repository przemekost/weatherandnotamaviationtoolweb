package com.przemyslawostrouch.domain;

/**
 * Created by Przemek on 2017-07-08.
 */
@lombok.Data
public class CityPair {

    private Origin origin;
    private Destination destination;
}
