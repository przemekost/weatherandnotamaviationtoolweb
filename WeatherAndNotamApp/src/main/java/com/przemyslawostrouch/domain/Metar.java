package com.przemyslawostrouch.domain;

import lombok.*;

import lombok.experimental.Tolerate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

/**
 * Created by Przemek on 2017-07-04.
 */
@lombok.Data
@Builder
@XmlRootElement(name ="METAR")
@XmlAccessorType(XmlAccessType.FIELD)
public class Metar {

    @Tolerate
    public Metar(){

    }

    @XmlElement(name = "raw_text")
    private String rawText;

    @XmlElement(name = "station_id")
    private String stationId;

    @XmlElement(name = "observation_time")
    private String observationTime;

    @XmlElement(name = "latitude")
    private float latitude;

    @XmlElement(name = "longitude")
    private float longitude;

    @XmlElement(name = "temp_c")
    private float tempC;    // in decimal Celcius degrees

    @XmlElement(name = "dewpoint_c")
    private float dewpointC;    // in decimal Celcius degrees

    @XmlElement(name = "wind_dir_degrees")
    private int windDirection;   // Degrees

    @XmlElement(name = "wind_speed_kt")
    private int windSpeed;   // Knots

    @XmlElement(name = "wind_gust_kt")
    private int windGusts;  // Knots

    @XmlElement(name = "visibility_statute_mi")
    private float visibility;   // StatuteMiles

    @XmlElement(name = "altim_in_hg")
    private float altimeter;    // inches of Hg

    @XmlElement(name = "sea_level_pressure_mb")
    private float seaLvlPressure;   // mb

    @XmlElement(name = "quality_control_flags")
    private String qualityControlFlags;

    @XmlElement(name = "wx_string")
    private String wxString;

    @XmlElement(name = "sky_cover")
    private String skyCover;

    @XmlElement(name = "cloud_base_ft_agl")
    private int cloudBaseAGL;   // cloud base above ground lvl feet

    @XmlElement(name = "flight_category")
    private String flightCategory;

    @XmlElement(name = "three_hr_pressure_tendency_mb")
    private float pressureTendency;     //Pressure change in the past 3 hours

    @XmlElement(name = "maxT_c")
    private float maxT;     // Maximum air temperature from the past 6 hours

    @XmlElement(name = "minT_c")
    private float minT;     // Minimum air temperature from the past 6 hours

    @XmlElement(name = "maxT24hr_c")
    private float maxT24;   //Maximum air temperature from the past 24 hours

    @XmlElement(name = "minT24hr_c")
    private float minT24;   //Minimum air temperature from the past 24 hours

    @XmlElement(name = "precip_in")
    private float precip;   //Liquid precipitation since the last regular METAR

    @XmlElement(name = "pcp3hr_in")
    private float pcp3;      //Liquid precipitation from the past 3 hours. 0.0005 in = trace precipitation

    @XmlElement(name = "pcp6hr_in")
    private float pcp6;     //Liquid precipitation from the past 6 hours. 0.0005 in = trace precipitation

    @XmlElement(name = "pcp24hr_in")
    private float pcp24;    //Liquid precipitation from the past 24 hours. 0.0005 in = trace precipitation

    @XmlElement(name = "snow_in")
    private float snow;     //Snow depth on the ground

    @XmlElement(name = "vert_vis_ft")
    private int verticalVisibility;     //Vertical Visibility

    @XmlElement(name = "metar_type")
    private String metarType;        //METAR or SPECI

    @XmlElement(name = "elevation_m")
    private float elevation;        //The elevation of the station that reported this METAR
}
