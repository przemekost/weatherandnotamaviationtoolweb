package com.przemyslawostrouch.domain;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;


/**
 * Created by Przemek on 2017-07-08.
 */
@lombok.Data
public class Origin {
    private String airport;
    @SerializedName("ETD")
    private String date;
}
