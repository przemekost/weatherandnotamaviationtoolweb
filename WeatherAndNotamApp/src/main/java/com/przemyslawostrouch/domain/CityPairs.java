package com.przemyslawostrouch.domain;

import com.google.gson.annotations.SerializedName;



import java.util.List;

/**
 * Created by Przemek on 2017-07-08.
 */
@lombok.Data
public class CityPairs {

    @SerializedName("city-pairs")
    private List<CityPair> cityPairList;

}
