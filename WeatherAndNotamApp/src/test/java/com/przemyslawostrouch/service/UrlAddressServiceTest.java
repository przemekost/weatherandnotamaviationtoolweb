//package com.przemyslawostrouch.service;
//
//import com.przemyslawostrouch.staticVariables.StaticValues;
//import com.przemyslawostrouch.staticVariables.StaticValuesForTest;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mockito;
//import org.powermock.core.classloader.annotations.PrepareForTest;
//import org.powermock.modules.junit4.PowerMockRunner;
//import org.junit.Assert;
//
//import java.util.HashMap;
//
//
///**
// * Created by Przemek on 2017-07-15.
// */
//
//@RunWith(PowerMockRunner.class)
//@PrepareForTest(UrlAddressService.class)
//public class UrlAddressServiceTest {
//
//
//    private HashMap<String, Long> departureAndArrivalTimeTest;
//    private UrlAddressService urlAddressService;
//    private TafAndMetarForCityPairService tams;
//
//    @Before
//    public void setUp() {
//        departureAndArrivalTimeTest = new HashMap<>();
//        tams = new TafAndMetarForCityPairService();
//        urlAddressService = new UrlAddressService();
//        departureAndArrivalTimeTest.put("departureTime", 1499264449L); //second arg dep time in sec
//        departureAndArrivalTimeTest.put("arrivalTime", 1499282449L); //second arg arr time in sec
//    }
//
//    @Test
//    public void createUrlForMetarTest() throws Exception {
//        TafAndMetarForCityPairService tamsSpy = Mockito.spy(tams);
//        urlAddressService.setTams(tamsSpy);
//        Mockito.when(tamsSpy.getDepartureAndArrivalTime()).thenReturn(departureAndArrivalTimeTest);
//
//        Mockito.when(tamsSpy.getOriginIcao()).thenReturn("EPWA");
//        Mockito.when(tamsSpy.getDestinationIcao()).thenReturn("EDDF");
//
//        System.out.println(urlAddressService);
//        System.out.println(urlAddressService.createUrlForMetar());
//
//        Assert.assertEquals(StaticValuesForTest.URL_ADDRESS_FOR_CITY_PAIR_METAR, urlAddressService.createUrlForMetar());
//
//
//    }
//
//    @Test
//    public void createUrlForTaf() {
//    }
//
//}