package com.przemyslawostrouch.controller;

import com.przemyslawostrouch.staticVariables.StaticValuesForTest;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

public class AirportdataTests extends FunctionalTest {

    @Test
    public void airportdataForEachCityTest(){
        given()
                .when()
                .get("random")
                .then()
                .body(containsString("airportId"))
                .body(containsString("name"))
                .body(containsString("city"))
                .body(containsString("country"))
                .body(containsString("altitude"))
                .body(containsString("iata"))
                .body(containsString("icao"))
                .body(containsString("dst"))
                .body(containsString("longitude"))
                .body(containsString("latitude"))
                .body(containsString("airport"))
                .body(containsString("tzDatabaseTime"))
                .body(containsString("type"))
                .body(containsString("source"))
                .statusCode(200);
    }

    @Test
    public void airportdataForKatowiceCityTest(){
        given()
                .when()
                .get(StaticValuesForTest.cityNameNumberOneForTest)
                .then()
                .body(containsString("670"))
                .body(containsString("Muchowiec Airport"))
                .body(containsString("Katowice"))
                .body(containsString("Poland"))
                .body(containsString("N"))
                .body(containsString("EPKM"))
                .body(containsString("50.2380981445312"))
                .body(containsString("19.03420066833496"))
                .body(containsString("909.0"))
                .body(containsString("1"))
                .body(containsString("Europe/Warsaw"))
                .body(containsString("airport"))
                .body(containsString("OurAirports"))
                .statusCode(200);
    }


    @Test
    public void airportdataForBostonCityTest(){
        given()
                .when()
                .get(StaticValuesForTest.cityNameNumberTwoForTest)
                .then()
                .body(containsString("3448"))
                .body(containsString("General Edward Lawrence Logan International Airport"))
                .body(containsString("Boston"))
                .body(containsString("United States"))
                .body(containsString("BOS"))
                .body(containsString("KBOS"))
                .body(containsString("42.36429977"))
                .body(containsString("-71.00520325"))
                .body(containsString("20.0"))
                .body(containsString("-5"))
                .body(containsString("America/New_York"))
                .body(containsString("airport"))
                .statusCode(200);
    }
    //METAR for Boston test
    @Test
    public void metarForBostonCityTest(){
        given()
                .when()
                .get("metar/"+StaticValuesForTest.cityNameNumberTwoForTest)
                .then()
                .body(containsString("raw_text"))
                .body(containsString("elevation_m"))
                .body(containsString("metar_type"))
                .body(containsString("visibility_statute_mi"))
                .body(containsString("BOS"))
                .body(containsString("KBOS"))
                .body(containsString("latitude"))
                .statusCode(200);
    }

    //TAF for Warsaw test
    @Test
    public void tafForEachCityTest(){
        given()
                .when()
                .get("taf/"+StaticValuesForTest.cityNameNumberThreeForTest)
                .then()
                .body(containsString("raw_text"))
                .body(containsString("elevation_m"))
                .body(containsString("forecast"))
                .body(containsString("bulletin_time"))
                .body(containsString("valid_time_from"))
                .body(containsString("valid_time_to"))
                .body(containsString("latitude"))
                .statusCode(200);
    }
//city pair WAW - FRA
    @Test
    public void tafAndMetarForCityPair(){
        given()
                .when()
                .get("citypairs/waw/fra/")
                .then()
                .body(containsString("EDDF"))
                .body(containsString("31012KT"))
                .body(containsString("30018G28KT"))
                .body(containsString("BKN005"))
                .body(containsString("TEMPO"))
                .body(containsString("SCT035"))
                .body(containsString("32004KT"))
                .statusCode(200);
    }

    //invalid cityPair null pointer exception (pair doesn't exist)
    @Test
    public void invalidParkingSpace() {
        given().when().get("citypairs/waw/bos/")
                .then().statusCode(500);
    }


    //printing last 10 request status test
    @Test
    public void requestStatusCode200(){
        given()
                .when()
                .get("request")
                .then()
                .statusCode(200);
    }
}
