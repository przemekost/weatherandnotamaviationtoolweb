package com.przemyslawostrouch.parsers;

import com.przemyslawostrouch.domain.TypeOfInformation;
import com.przemyslawostrouch.staticVariables.StaticValuesForTest;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

/**
 * Created by Przemek on 2017-07-11.
 */
public class XmlToJsonTest {

    private XmlToJson xmlToJson;
    private JSONObject jsonObjectTest;

    @Before
    public void setup()  {
        xmlToJson = new XmlToJson();


    }
    @Test
    public void xmlToJson() throws Exception {
    }

    @Test
    public void ifCheckMetarsThenReturnNumberOfNotices() throws JSONException {
        jsonObjectTest = new JSONObject(StaticValuesForTest.jsonMetarStringForTest);
        assertThat(1).isEqualTo(xmlToJson.numberOfNoticeForAirport(jsonObjectTest));
    }

    @Test
    public void takeARawTextElementFromNotice() throws JSONException {
        jsonObjectTest = new JSONObject(StaticValuesForTest.jsonMetarStringCityPairForTest);
        String rawTextForTest = "TAF EDDF 051734Z 0518/0624 33008KT CAVOK BECMG 0518/0521 03004KT BECMG 0610/0612 22005KT PROB30 TEMPO 0616/0624 22015G25KT TSRA BKN045CB"
                +"\n"
                + "TAF EPWA 051730Z 0518/0618 32005KT CAVOK PROB40 0602/0604 3000 BR"
                +"\n";
        assertThat(rawTextForTest).isEqualTo(xmlToJson.takeARawTextElementFromNotice("TAF", jsonObjectTest));
    }

}