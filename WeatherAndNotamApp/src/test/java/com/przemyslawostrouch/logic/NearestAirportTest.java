package com.przemyslawostrouch.logic;

import com.przemyslawostrouch.domain.Airport;
import com.przemyslawostrouch.domain.City;
import com.przemyslawostrouch.domain.CityAirportDistance;
import org.junit.Test;


import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created by Przemek on 2017-06-24.
 */
public class NearestAirportTest {

    private NearestAirport nearestAirport;
    private City katowiceCity;
    private Airport katowiceAirport;
    private Airport frankfurtAirport;
    private Airport pulkovoAirport;
    private CityAirportDistance cityAirportDistance;
    private CityAirportDistance cityAirportDistance1;
    private CityAirportDistance cityAirportDistance2;
    private String searchedCityUpperCase;

    public void setup() {


        searchedCityUpperCase = "Katowice";

        nearestAirport = new NearestAirport();

        katowiceCity = City.builder()
                .name("Katowice")
                .latitude(50.26489189999999)
                .longitude(19.0237815)
                .build();

        katowiceAirport = Airport.builder()
                .name("Muchowiec Airport")
                .longitude(19.03420066833496)
                .latitude(50.23809814453125)
                .build();

        frankfurtAirport = Airport.builder()
                .name("Frankfurt am Main International Airport")
                .longitude(8.5705556)
                .latitude(50.0333333)
                .build();

        pulkovoAirport = Airport.builder()
                .name("Pulkovo Airport")
                .longitude(30.262500762939453)
                .latitude(59.80030059814453)
                .build();

        cityAirportDistance = CityAirportDistance.builder()
                .airportName(katowiceAirport.getName())
                .searchedCityName(searchedCityUpperCase)
                .distanceBetweenSearchedCityAndAirport(1.6565764398773335)
                .build();

        cityAirportDistance1 = CityAirportDistance.builder()
                .airportName(frankfurtAirport.getName())
                .searchedCityName(searchedCityUpperCase)
                .distanceBetweenSearchedCityAndAirport(401.81041784523916)
                .build();

        cityAirportDistance2 = CityAirportDistance.builder()
                .airportName(pulkovoAirport.getName())
                .searchedCityName(searchedCityUpperCase)
                .distanceBetweenSearchedCityAndAirport(688.4011751978946)
                .build();
    }

    @Test
    public void isNearestAirportNotNull() {
        setup();
        assertThat(nearestAirport).isNotNull();
    }

    @Test
    public void whenTypedCityReturnCityWithFirstLetterUpperCase() {
        setup();
        String searchedCity = "katowice";
        assertThat(nearestAirport.searchedCityToUpperCase(searchedCity)).isEqualTo("Katowice");
    }


    @Test
    public void forGivenCityAndAerodromeCoordinatesReturnDistance() {
        setup();
        //City Gdansk
        City gdanskCity = City.builder()
                .name("Gdansk")
                .latitude(54.35202520000001)
                .longitude(18.6466384)
                .build();
        Airport gdanskAirport = Airport.builder()
                .name("Gdańsk Lech Wałęsa Airport")
                .latitude(54.377601623535156)
                .longitude(18.46619987487793)
                .build();

        assertThat(nearestAirport.calculateDistance(gdanskCity, gdanskAirport)).isEqualTo(6.491638306515884);
    }

    @Test
    public void forGivenCityNameCalculateDistanceBetweenAllAirports() {
        setup();
        List<Airport> listOfAirportsForTest = new ArrayList<>();

        listOfAirportsForTest.add(katowiceAirport);
        listOfAirportsForTest.add(frankfurtAirport);
        listOfAirportsForTest.add(pulkovoAirport);


        assertThat(nearestAirport.calculateDistanceBetweenAllAirportsAndSelectedCity(listOfAirportsForTest, katowiceCity))
                .contains(cityAirportDistance, cityAirportDistance1, cityAirportDistance2);

    }

    @Test
    public void sortListDistancesAndReturnNearestAirport() {
        setup();

        List<CityAirportDistance> cityAirportDistancesListTest = new ArrayList<>();


        cityAirportDistancesListTest.add(cityAirportDistance);
        cityAirportDistancesListTest.add(cityAirportDistance1);
        cityAirportDistancesListTest.add(cityAirportDistance2);

        assertThat(nearestAirport.createListOfNearestAirports(katowiceCity)).contains(cityAirportDistancesListTest.get(0)
                , cityAirportDistancesListTest.get(1)
                , cityAirportDistancesListTest.get(2));

    }

//    @Test
//    void forSelectedAirportByUserReturnJson() {
//        setup();
//
//        String jsonString = "{\"airportId\":670,\"name\":\"Muchowiec Airport\",\"city\":\"Katowice\",\"country\":\"Poland\"" +
//                ",\"iata\":\"N\",\"icao\":\"EPKM\",\"latitude\":50.23809814453125,\"longitude\":19.03420066833496,\"altitude\":909.0" +
//                ",\"timeZone\":\"1\",\"dst\":\"E\",\"tzDatabaseTime\":\"Europe/Warsaw\",\"type\":\"airport\",\"source\":\"OurAirports\"}";
//
//        assertThat(nearestAirport.parseToJson()).isEqualTo(jsonString);
//
//    }
}