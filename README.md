# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Weather and airport information web application
* v2.1


### How to start app ###

Start WeatherAppApplication main class and use e.g Postman for request GET method on:
1) In order to search nearest airport for given city and print informations about it:
http://localhost:8080/airportdata/{city name}

{city name} --> type any city name (google api used)

e.g nearest airport from the city: Boston
http://localhost:8080/airportdata/boston

2) In order to search METAR for nearest and print it:
http://localhost:8080/airportdata/metar/{city name}

{city name} --> type any city name (google api used)

e.g METAR for the nearest airport for: Frankfurt
http://localhost:8080/airportdata/metar/frankfurt


3) In order to search TAF for nearest and print it:
http://localhost:8080/airportdata/taf/{city name}

{city name} --> type any city name (google api used)

e.g TAF for the nearest airport from the city: Warszawa
http://localhost:8080/airportdata/taf/warszawa

4) In order to search raw format METAR and taf TAF specific city pairs and print it:
http://localhost:8080/airportdata/citypairs/{originIata}/{destinationIata}

Hardcoded citypairs just for demo (IATA codes):
    origin - destination
a)  WAW    -    FRA
b)  FRA    -    GDN
c)  GDN    -    WAW

e.g. city pair: Gdansk - Warszawa
http://localhost:8080/airportdata/citypairs/gdn/waw

5) In order to return last 10 business oriented rest calls:
http://localhost:8080/airportdata/request

----------------------------

* Repo owner: Przemysław Ostrouch
* contact via e-mail przemekost@gmail.com